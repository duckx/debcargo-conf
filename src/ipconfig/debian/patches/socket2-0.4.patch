This patch is based on the upstream commit described below, adapted to match
the line endings and Cargo.toml format used in the Debian package by Peter
Michael Green.

commit bbf91d2230b546f7d37ad053a0b932e411b89a30
Author: conblem <ramonwehrli@gmail.com>
Date:   Mon Jul 5 17:48:04 2021 +0200

    Update socket2 to v0.4 (#41)
    
    * Update socket2 to v0.4
    
    Co-authored-by: Thomas de Zeeuw <thomasdezeeuw@gmail.com>

Index: ipconfig/src/adapter.rs
===================================================================
--- ipconfig.orig/src/adapter.rs
+++ ipconfig/src/adapter.rs
@@ -1,13 +1,14 @@
 #![allow(clippy::cast_ptr_alignment)]
 
 use std;
+use std::convert::TryFrom;
 use std::ffi::CStr;
 use std::net::IpAddr;
 
 use crate::error::*;
 use socket2;
 use widestring::WideCString;
-use winapi::shared::winerror::{ERROR_SUCCESS, ERROR_BUFFER_OVERFLOW};
+use winapi::shared::winerror::{ERROR_BUFFER_OVERFLOW, ERROR_SUCCESS};
 use winapi::shared::ws2def::AF_UNSPEC;
 use winapi::shared::ws2def::SOCKADDR;
 
@@ -240,16 +241,18 @@ unsafe fn get_adapter(adapter_addresses_
 }
 
 unsafe fn socket_address_to_ipaddr(socket_address: &SOCKET_ADDRESS) -> IpAddr {
-    let sockaddr = socket2::SockAddr::from_raw_parts(
-        socket_address.lpSockaddr as *const SOCKADDR,
-        socket_address.iSockaddrLength,
-    );
-
-    // Could be either ipv4 or ipv6
-    sockaddr
-        .as_inet()
-        .map(|s| IpAddr::V4(*s.ip()))
-        .unwrap_or_else(|| IpAddr::V6(*sockaddr.as_inet6().unwrap().ip()))
+    let (_, sockaddr) = socket2::SockAddr::init(|storage, length| {
+        let sockaddr_length = usize::try_from(socket_address.iSockaddrLength).unwrap();
+        assert!(sockaddr_length <= std::mem::size_of_val(&*storage));
+        let dst: *mut u8 = storage.cast();
+        let src: *const u8 = socket_address.lpSockaddr.cast();
+        dst.copy_from_nonoverlapping(src, sockaddr_length);
+        *length = socket_address.iSockaddrLength;
+        Ok(())
+    })
+    .unwrap();
+
+    sockaddr.as_socket().map(|s| s.ip()).unwrap()
 }
 
 unsafe fn get_dns_servers(
Index: ipconfig/Cargo.toml
===================================================================
--- ipconfig.orig/Cargo.toml
+++ ipconfig/Cargo.toml
@@ -23,7 +23,7 @@ keywords = ["ipconfig", "network", "adap
 license = "MIT/Apache-2.0"
 repository = "https://github.com/liranringel/ipconfig"
 [target."cfg(windows)".dependencies.socket2]
-version = "^0.3.1"
+version = "^0.4.0"
 
 [target."cfg(windows)".dependencies.widestring]
 version = "^0.4"
